#!/usr/bin/python

import mysql.connector
import cgitb
import sys
import urllib2
import urllib
import os
import json

reload(sys)
sys.setdefaultencoding("utf-8")
cgitb.enable()
def GetHTTPS(url):
    xResponse = urllib2.urlopen("http://gen.meltwater.com/httpsProxy/httpsget.php?url="+urllib2.quote(url))
    return xResponse.read()
def postHTTPS(url,data):
    values = urllib.urlencode(data)
    outUrl = urllib.quote(url,safe='')
    req = urllib2.Request("http://gen.meltwater.com/httpsProxy/httpspost.php?url="+outUrl, values)
    response = urllib2.urlopen(req)
    the_page = response.read()
    return the_page
def patchHTTPS(url,authorization,data):
    dataObj = {}
    dataObj["patchData"] = data
    values = urllib.urlencode(dataObj)
    values = values.replace("%5Cr","%0D")
    values = values.replace("%5Cn","%0A")
    outUrl = urllib.quote(url,safe='')
    outAuthorization = urllib.quote(authorization,safe='')
    req = urllib2.Request("http://gen.meltwater.com/httpsProxy/httpspatch.php?url="+outUrl+"&authorization="+outAuthorization, values)
    response = urllib2.urlopen(req)
    the_page = response.read()
    addLog(values,"Patch Request")
    addLog(the_page,"Patch Response")
    return the_page
def addLog(logEntry,entryType):
    cnx = mysql.connector.connect(user=mySqlUserId, password=mySqlPassword, database='jirasync')
    cursor = cnx.cursor()
    cursor.execute("INSERT INTO Log (logEntry,entryType,Application) VALUES (%s,%s,%s)", (logEntry,entryType,
                                                                                          os.path.basename(__file__)))
    cursor.close()
    cnx.close()
try:
    apiKey = "a1c62771-d15d-4eb0-96a8-eebc06c22d15"
    internalStatusPageId = "bxzt6x2rdl2t"
    externalStatusPageId = "w0w7mxxmf2r6"
    mySqlUserId = "root"
    mySqlPassword = "supp0rt"
    pageId = internalStatusPageId
    statusPageJSON = GetHTTPS("https://api.statuspage.io/v1/pages/"+pageId+"/components.json?api_key="+apiKey)
    statusPageObj = json.loads(statusPageJSON)
    applicationSPStatus=""
    applicationSPid=""
    DBApplicationImpact=""
    searchPStatus=""
    searchSPid=""
    DBSearchImpact=""
    reportSPStatus=""
    reportSPid=""
    DBReportsImpact=""
    for component in statusPageObj:
        if component["name"] == "Application":
            applicationSPid = component["id"]
            applicationSPStatus = component["status"]
        if component["name"] == "Search":
            searchSPid = component["id"]
            searchSPStatus = component["status"]
        if component["name"] == "Report":
            reportSPid  =component["id"]
            reportSPStatus = component["status"]
    cnx = mysql.connector.connect(user=mySqlUserId, password=mySqlPassword, database='jirasync')
    cursor = cnx.cursor()
    cursor.execute("SELECT applicationImpact from Incidents where visability in ('External','Meltwater') and sevStatus "
                   "!= 'resolved' order by FIELD(applicationImpact,'major_outage','partial_outage',"
                   "'degraded_performance','operational') LIMIT 1;")
    for (applicationImpact,) in cursor:
        DBApplicationImpact = applicationImpact
    cursor.close()
    cursor = cnx.cursor()
    cursor.execute("SELECT searchImpact from Incidents where visability in ('External','Meltwater') and sevStatus "
                   "!= 'resolved' order by FIELD(searchImpact,'major_outage','partial_outage',"
                   "'degraded_performance','operational') LIMIT 1;")
    for (searchImpact,) in cursor:
        DBSearchImpact=searchImpact
    cursor.close()
    cursor = cnx.cursor()
    cursor.execute("SELECT reportsImpact from Incidents where visability in ('External','Meltwater') and sevStatus "
                   "!= 'resolved' order by FIELD(reportsImpact,'major_outage','partial_outage',"
                   "'degraded_performance','operational') LIMIT 1;")
    for (reportsImpact,) in cursor:
        DBReportsImpact=reportsImpact
    cursor.close()
    cnx.close()
    if applicationSPStatus!=DBApplicationImpact:
        outData = []
        outData.append(str("component[status]="+DBApplicationImpact))
        postResponse = patchHTTPS("https://api.statuspage.io/v1/pages/"+pageId+"/components/"+applicationSPid+".json",apiKey,outData)
    if searchSPStatus!=DBSearchImpact:
        outData = []
        outData.append(str("component[status]="+DBSearchImpact))
        postResponse = patchHTTPS("https://api.statuspage.io/v1/pages/"+pageId+"/components/"+searchSPid+".json",apiKey,outData)
    if reportSPStatus!=DBReportsImpact:
        outData = []
        outData.append(str("component[status]="+DBReportsImpact))
        postResponse = patchHTTPS("https://api.statuspage.io/v1/pages/"+pageId+"/components/"+reportsSPid+".json",apiKey,outData)
#If there is a runtime error, log it
except Exception as inst:
    addLog("ERROR: "+unicode(inst),"Error")

