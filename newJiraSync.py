#!/usr/bin/python

import mysql.connector
import cgitb
import sys
from jira import JIRA
import urllib2
import urllib
import os
import json

reload(sys)
sys.setdefaultencoding("utf-8")
cgitb.enable()
def GetHTTPS(url):
    xResponse = urllib2.urlopen("http://gen.meltwater.com/httpsProxy/httpsget.php?url="+urllib2.quote(url))
    return xResponse.read()
def postHTTPS(url,data):
    values = urllib.urlencode(data)
    outUrl = urllib.quote(url,safe='')
    req = urllib2.Request("http://gen.meltwater.com/httpsProxy/httpspost.php?url="+outUrl, values)
    response = urllib2.urlopen(req)
    the_page = response.read()
    return the_page
def patchHTTPS(url,authorization,data):
    dataObj = {}
    dataObj["patchData"] = data
    values = urllib.urlencode(dataObj)
    values = values.replace("%5Cr","%0D")
    values = values.replace("%5Cn","%0A")
    outUrl = urllib.quote(url,safe='')
    outAuthorization = urllib.quote(authorization,safe='')
    req = urllib2.Request("http://gen.meltwater.com/httpsProxy/httpspatch.php?url="+outUrl+"&authorization="+outAuthorization, values)
    response = urllib2.urlopen(req)
    the_page = response.read()
    return the_page
#get a list of all keys from database
def getAllKeysFromDB():
    output = []
    cnx = mysql.connector.connect(user=mySqlUserId, password=mySqlPassword, database='jirasync')
    cursor = cnx.cursor()
    query = ("SELECT jiraKey,sevLevel FROM Incidents")
    cursor.execute(query)
    for (jiraKey,sevLevel) in cursor:
        output.append(jiraKey)
    cursor.close()
    cnx.close()
    return output
#get a list of all keys unresolved keys from database
def getAllUnresolvedKeysFromDB():
    output = []
    cnx = mysql.connector.connect(user=mySqlUserId, password=mySqlPassword, database='jirasync')
    cursor = cnx.cursor()
    query = ("SELECT jiraKey,sevLevel FROM Incidents WHERE sevStatus!='resolved'")
    cursor.execute(query)
    for (jiraKey,sevLevel) in cursor:
        output.append(jiraKey)
    cursor.close()
    cnx.close()
    return output
#add entry to Log
def addLog(logEntry,entryType):
    cnx = mysql.connector.connect(user=mySqlUserId, password=mySqlPassword, database='jirasync')
    cursor = cnx.cursor()
    cursor.execute("INSERT INTO Log (logEntry,entryType,Application) VALUES (%s,%s,%s)", (logEntry,entryType,
                                                                                          os.path.basename(__file__)))
    cursor.close()
    cnx.close()
 #add Incident to the Database
def addIncidentToDatabase(thisJIRASev):
    jiraKey = thisJIRASev.key
    print thisJIRASev.fields
    sevLevel = JiraPrioritytoStatuspage(unicode(thisJIRASev.fields.customfield_12090))
    sevStatus = JiraStatustoStatuspage(unicode(thisJIRASev.fields.status))
    visability = unicode(thisJIRASev.fields.customfield_12087)
    platform = unicode(thisJIRASev.fields.customfield_12088)
    if visability == "External" and platform != "Haakon":
        visability = "Meltwater"
    applicationImpact = JiraImpacttoStatuspage(unicode(thisJIRASev.fields.customfield_12783))
    searchImpact = JiraImpacttoStatuspage(unicode(thisJIRASev.fields.customfield_12784))
    reportsImpact = JiraImpacttoStatuspage(unicode(thisJIRASev.fields.customfield_12785))
    name = unicode(thisJIRASev.fields.summary)
    workaround = unicode(thisJIRASev.fields.customfield_12089)
    description = unicode(thisJIRASev.fields.description)
    description = description.replace("\\r\\n", "\r\n")
    cnx = mysql.connector.connect(user=mySqlUserId, password=mySqlPassword, database='jirasync')
    cursor = cnx.cursor()
    cursor.execute("INSERT INTO Incidents (jiraKey,sevLevel,sevStatus,visability,externalStatusPageId,"
                   "internalStatusPageId,applicationImpact,searchImpact,reportsImpact,JIRAUpdate,internalUpdate,"
                   "externalUpdate,name,workaround,description) VALUES (%s,%s,%s,%s,NULL,NULL,%s,%s,%s,CURRENT_TIMESTAMP,"
                   "NULL,NULL,%s,%s,%s)",(jiraKey,sevLevel,sevStatus,visability,applicationImpact,searchImpact,
                    reportsImpact,name,workaround,description))
    addLog(cursor.statement,"DBEntry")
    cursor.close()
    cnx.close()
#convert JIRA priority to Statuspage Sev Priority
def JiraPrioritytoStatuspage(jiraPriority):
    if jiraPriority == "1":
        return "critical"
    elif jiraPriority == "2":
        return "major"
    elif jiraPriority == "3":
        return "minor"
#convert JIRA incident status to Statuspage incident status
def JiraStatustoStatuspage(jiraStatus):
    if (jiraStatus == "Open") or (jiraStatus == "In Progress"):
        return "investigating"
    if jiraStatus == "Root Cause Identified":
        return "identified"
    if jiraStatus == "Monitoring":
        return "monitoring"
    if (jiraStatus == "Resolved") or (jiraStatus == "Closed"):
        return "resolved"
#convert JIRA impacts to Statuspage impacts
def JiraImpacttoStatuspage(jiraImpact):
    if (jiraImpact == "Operational" or jiraImpact == "None"):
        return "operational"
    if jiraImpact == "Degraded Performance":
        return "degraded_performance"
    if jiraImpact == "Partial Outage":
        return "partial_outage"
    if jiraImpact == "Major Outage":
        return "major_outage"
#update an existing Severity
def updateJiraSev(thisJIRASev):
    MyUpdateJira = False
    MyUpdateWorkaround = False
    jiraKey = thisJIRASev.key
    sevLevel = JiraPrioritytoStatuspage(unicode(thisJIRASev.fields.customfield_12090))
    sevStatus = JiraStatustoStatuspage(unicode(thisJIRASev.fields.status))
    visability = unicode(thisJIRASev.fields.customfield_12087)
    platform = unicode(thisJIRASev.fields.customfield_12088)
    if visability == "External" and platform != "Haakon":
        visability = "Meltwater"
    applicationImpact = JiraImpacttoStatuspage(unicode(thisJIRASev.fields.customfield_12783))
    searchImpact = JiraImpacttoStatuspage(unicode(thisJIRASev.fields.customfield_12784))
    reportsImpact = JiraImpacttoStatuspage(unicode(thisJIRASev.fields.customfield_12785))
    name = unicode(thisJIRASev.fields.summary)
    workaround = unicode(thisJIRASev.fields.customfield_12089)
    description = unicode(thisJIRASev.fields.description)
    description = description.replace("\\r\\n", "\r\n")
    cnx = mysql.connector.connect(user=mySqlUserId, password=mySqlPassword, database='jirasync')
    cursor = cnx.cursor()
    #first, find out if the record needs to be updated
    cursor.execute(("SELECT IF(sevLevel!=%s OR sevStatus!=%s OR visability!=%s OR applicationImpact!=%s OR "
                   "searchImpact!=%s OR reportsImpact!=%s OR name!=%s OR description!=%s,'yes','no') AS 'updateJIRA', IF("
                   "workaround!=%s,'yes','no') AS 'updateWorkaround' FROM Incidents WHERE jiraKey=%s"),(sevLevel,sevStatus,
                    visability,applicationImpact,searchImpact,reportsImpact,name,description,workaround,jiraKey))
    addLog(cursor.statement,"DBEntry")
    for (updateJIRA,updateWorkaround) in cursor:
        if updateJIRA == "yes":
            MyUpdateJira = True
        if updateWorkaround == "yes":
            MyUpdateWorkaround = True
    cursor.close()
    if MyUpdateJira == True:
        cursor2 = cnx.cursor()
        cursor2.execute(("UPDATE Incidents SET sevLevel=%s,sevStatus=%s,visability=%s,applicationImpact=%s,"
                   "searchImpact=%s,reportsImpact=%s,name=%s,description=%s,JIRAUpdate=CURRENT_TIMESTAMP WHERE jiraKey=%s"),(sevLevel,
                    sevStatus,visability,applicationImpact,searchImpact,reportsImpact,name,description,jiraKey))
        addLog(cursor2.statement,"DBEntry")
        cursor2.close()
    if MyUpdateWorkaround == True:
        cursor2 = cnx.cursor()
        cursor2.execute(("UPDATE Incidents SET workaround=%s,workaroundUpdate=CURRENT_TIMESTAMP WHERE jiraKey=%s"),(
                    workaround,jiraKey))
        addLog(cursor2.statement,"DBEntry")
        cursor2.close()
    cnx.close()
#add or update comments for each severity in JIRA
def checkComments(jira,thisJIRASev):
    cnx = mysql.connector.connect(user=mySqlUserId, password=mySqlPassword, database='jirasync')
    for thisComment in jira.comments(thisJIRASev):
        jiraKey=thisJIRASev.key
        jiraCommentId=thisComment.id
        commentText=thisComment.body
        commentText=commentText.replace("{color:white}{color}","")
        cursor = cnx.cursor()
        cursor.execute(("SELECT jiraKey,jiraCommentId FROM Comments WHERE jiraKey=%s AND jiraCommentId=%s AND "
                       "commentText=%s"),(jiraKey,jiraCommentId,commentText))
        addLog(cursor.statement,"DBEntry")
        count = 0
        for thisobject in cursor:
            count+=1
        cursor.close()
        if(count==0):
            cursor2 = cnx.cursor()
            cursor2.execute(("INSERT INTO Comments (jiraKey,jiraCommentId,commentText) VALUES (%s,%s,%s) ON DUPLICATE KEY"
                            " UPDATE commentText=%s,JIRATime=CURRENT_TIMESTAMP"),(jiraKey,jiraCommentId,commentText,
                           commentText))
            cursor2.close()
    cnx.close()
#The Main Program
try:
    thisJira = JIRA(server='http://jira.meltwater.com',options=None,basic_auth=("status.page","mwstatus"))
    # Parameters
    apiKey = "a1c62771-d15d-4eb0-96a8-eebc06c22d15"
    internalStatusPageId = "bxzt6x2rdl2t"
    externalStatusPageId = "w0w7mxxmf2r6"
    mySqlUserId = "root"
    mySqlPassword = "supp0rt"

    # Get a list of all open Severities in JIRA
    allOpenJIRASevs = thisJira.search_issues("project = SEV AND issuetype = Severity AND status in (Open, "
                                             "\"In Progress\", Monitoring, \"Root Cause Identified\")")
    allSevsInDB = getAllKeysFromDB()
    # Add new Severities to Database
    for thisOpenJIRASev in allOpenJIRASevs:
        key = thisOpenJIRASev.key
        if key in allSevsInDB:
            A=0
        else:
            addIncidentToDatabase(thisOpenJIRASev)
    # Get a list of all open Severities in Database
    allOpenSevsinDB = getAllUnresolvedKeysFromDB()
    allJIRASevstoUpdate = []
    if len(allOpenSevsinDB)>0:
        queryToJira = "KEY IN ("
        for thisOpenSevinDB in allOpenSevsinDB:
            queryToJira += "'"+thisOpenSevinDB+"',"
        queryToJira=queryToJira[:-1]
        queryToJira+=")"
        allJIRASevstoUpdate = thisJira.search_issues(queryToJira)
    for thisJIRASevtoUpdate in allJIRASevstoUpdate:
        updateJiraSev(thisJIRASevtoUpdate)
        checkComments(thisJira,thisJIRASevtoUpdate)
#If there is a runtime error, log it
except Exception as inst:
    addLog("ERROR: "+unicode(inst),"Error")

