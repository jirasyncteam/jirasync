#!/usr/bin/python

import mysql.connector
import cgitb
import sys
import urllib2
import urllib
import os
import json

reload(sys)
sys.setdefaultencoding("utf-8")
cgitb.enable()
class Incident:
    def __init__(self,jiraKey,sevLevel,sevStatus,visability,externalStatusPageId,internalStatusPageId,applicationImpact,
                 searchImpact,reportsImpact,JIRAUpdate,internalUpdate,externalUpdate,name,workaround,workaroundUpdate,
                 workaroundID,description):
        self.jiraKey = jiraKey
        self.sevLevel = sevLevel
        self.sevStatus = sevStatus
        self.visability = visability
        self.externalStatusPageId = externalStatusPageId
        self.internalStatusPageId = internalStatusPageId
        self.applicationImpact = applicationImpact
        self.searchImpact = searchImpact
        self.reportsImpact = reportsImpact
        self.JIRAUpdate = JIRAUpdate
        self.internalUpdate = internalUpdate
        self.externalUpdate = externalUpdate
        self.name = name
        self.workaround = workaround
        self.workaroundUpdate = workaroundUpdate
        self.workaroundID = workaroundID
        self.description = description
class Comment:
    def __init__(self,commentText,jiraKey,jiraCommentId,internalStatusPageId):
        self.commentText = commentText
        self.jiraKey = jiraKey
        self.jiraCommentId = jiraCommentId
        self.internalStatusPageId = internalStatusPageId
def GetHTTPS(url):
    xResponse = urllib2.urlopen("http://gen.meltwater.com/httpsProxy/httpsget.php?url="+urllib2.quote(url))
    return xResponse.read()
def postHTTPS(url,data):
    values = urllib.urlencode(data)
    outUrl = urllib.quote(url,safe='')
    req = urllib2.Request("http://gen.meltwater.com/httpsProxy/httpspost.php?url="+outUrl, values)
    response = urllib2.urlopen(req)
    the_page = response.read()
    addLog(values,"Post Request")
    addLog(the_page,"Post Response")
    return the_page
def patchHTTPS(url,authorization,data):
    dataObj = {}
    dataObj["patchData"] = data
    values = urllib.urlencode(dataObj)
    values = values.replace("%5Cr","%0D")
    values = values.replace("%5Cn","%0A")
    outUrl = urllib.quote(url,safe='')
    outAuthorization = urllib.quote(authorization,safe='')
    req = urllib2.Request("http://gen.meltwater.com/httpsProxy/httpspatch.php?url="+outUrl+"&authorization="+outAuthorization, values)
    response = urllib2.urlopen(req)
    the_page = response.read()
    addLog(values,"Patch Request")
    addLog(the_page,"Patch Response")
    return the_page
def addLog(logEntry,entryType):
    cnx = mysql.connector.connect(user=mySqlUserId, password=mySqlPassword, database='jirasync')
    cursor = cnx.cursor()
    cursor.execute("INSERT INTO Log (logEntry,entryType,Application) VALUES (%s,%s,%s)", (logEntry,entryType,
                                                                                          os.path.basename(__file__)))
    cursor.close()
    cnx.close()

try:
    apiKey = "a1c62771-d15d-4eb0-96a8-eebc06c22d15"
    internalStatusPageId = "bxzt6x2rdl2t"
    externalStatusPageId = "w0w7mxxmf2r6"
    mySqlUserId = "root"
    mySqlPassword = "supp0rt"
    pageId = internalStatusPageId
    #get list of sevs that need to be ADDED
    incidentsToAdd = []
    cnx = mysql.connector.connect(user=mySqlUserId, password=mySqlPassword, database='jirasync')
    cursor = cnx.cursor()
    cursor.execute("SELECT jiraKey,sevLevel,sevStatus,visability,externalStatusPageId,internalStatusPageId,"
                   "applicationImpact,searchImpact,reportsImpact,JIRAUpdate,internalUpdate,externalUpdate,name,"
                   "workaround,workaroundUpdate,workaroundID,description FROM Incidents WHERE visability IN ('External',"
                   "'Meltwater') AND internalStatusPageId IS NULL")
    addLog(cursor.statement,"DBEntry")
    for (jiraKey,sevLevel,sevStatus,visability,externalStatusPageId,internalStatusPageId,applicationImpact,searchImpact,
         reportsImpact,JIRAUpdate,internalUpdate,externalUpdate,name,workaround,workaroundUpdate,workaroundID,
         description) in cursor:
        thisIncident = Incident(jiraKey,sevLevel,sevStatus,visability,externalStatusPageId,internalStatusPageId,
                                applicationImpact,searchImpact,reportsImpact,JIRAUpdate,internalUpdate,externalUpdate,
                                name,workaround,workaroundUpdate,workaroundID,description)
        incidentsToAdd.append(thisIncident)
    cursor.close()
    #Add the Sevs that need to be ADDED
    for thisIncidentToAdd in incidentsToAdd:
        outURL = "https://api.statuspage.io/v1/pages/"+pageId+"/incidents.json?api_key="+apiKey
        outData = {}
        outData["incident[name]"] = "["+thisIncidentToAdd.jiraKey+"] "+thisIncidentToAdd.name
        outData["incident[status]"] = thisIncidentToAdd.sevStatus
        outData["incident[message]"] = thisIncidentToAdd.description
        outData["incident[impact_override]"] = thisIncidentToAdd.sevLevel
        postResponse = postHTTPS(outURL,outData)
        addLog(postResponse,"PostNewIssue")
        postResponseObj = json.loads(postResponse)
        internalId=postResponseObj["id"]
        cursor = cnx.cursor()
        cursor.execute(("UPDATE Incidents SET internalUpdate = CURRENT_TIMESTAMP,internalStatusPageId=%s WHERE "
                        "jiraKey=%s"),(internalId,thisIncidentToAdd.jiraKey))
        addLog(cursor.statement,"DBEntry")
        cursor.close()
    #get list of sevs that need to be Updated
    incidentsToUpdate = []
    cursor = cnx.cursor()
    cursor.execute("SELECT jiraKey,sevLevel,sevStatus,visability,externalStatusPageId,internalStatusPageId,"
                   "applicationImpact,searchImpact,reportsImpact,JIRAUpdate,internalUpdate,externalUpdate,name,"
                   "workaround,workaroundUpdate,workaroundID,description FROM Incidents WHERE visability IN ('External',"
                   "'Meltwater') AND JIRAUpdate>internalUpdate")
    addLog(cursor.statement,"DBEntry")
    for (jiraKey,sevLevel,sevStatus,visability,externalStatusPageId,internalStatusPageId,applicationImpact,searchImpact,
         reportsImpact,JIRAUpdate,internalUpdate,externalUpdate,name,workaround,workaroundUpdate,workaroundID,
         description) in cursor:
        thisIncident = Incident(jiraKey,sevLevel,sevStatus,visability,externalStatusPageId,internalStatusPageId,applicationImpact,searchImpact,
         reportsImpact,JIRAUpdate,internalUpdate,externalUpdate,name,workaround,workaroundUpdate,workaroundID,
         description)
        incidentsToUpdate.append(thisIncident)
    cursor.close()
    #send a Patch for each incident that needs to be updated
    for thisIncidentToUpdate in incidentsToUpdate:
        patchData = []
        patchData.append(str("incident[name]="+"["+thisIncidentToUpdate.jiraKey+"] "+thisIncidentToUpdate.name))
        patchData.append(str("incident[status]="+thisIncidentToUpdate.sevStatus))
        patchData.append(str("incident[impact_override]="+thisIncidentToUpdate.sevLevel))
        messageText = ""
        messageCommentId = ""
        cursor = cnx.cursor()
        cursor.execute(("SELECT jiraKey,JIRATime,commentText,jiraCommentId FROM Comments WHERE jiraKey=%s AND internalCommentId IS NULL"
                       " ORDER BY JIRATime ASC LIMIT 1"),(thisIncidentToUpdate.jiraKey,))
        addLog(cursor.statement,"DBEntry")
        for (jiraKey,JIRATime,commentText,jiraCommentId) in cursor:
            messageText = commentText
            messageCommentId = jiraCommentId
        cursor.close()
        if messageText != "":
            patchData.append(str("incident[message]="+messageText))
        postResponse = patchHTTPS("https://api.statuspage.io/v1/pages/"+pageId+"/incidents/"+thisIncidentToUpdate.internalStatusPageId+".json",apiKey,patchData)
        postResponseObj = json.loads(postResponse)
        internalId = postResponseObj["incident_updates"][0]["id"]
        if messageText != "":
            cursor = cnx.cursor()
            cursor.execute(("UPDATE Comments Set internalCommentId=%s,internalTime=CURRENT_TIMESTAMP where jiraKey=%s AND"
                           " jiraCommentId=%s"),(internalId,thisIncidentToUpdate.jiraKey,messageCommentId))
            addLog(cursor.statement,"DBEntry")
            cursor.close()
        cursor = cnx.cursor()
        cursor.execute(("UPDATE Incidents SET internalUpdate = CURRENT_TIMESTAMP WHERE "
                        "jiraKey=%s"),(thisIncidentToUpdate.jiraKey,))
        addLog(cursor.statement,"DBEntry")
        cursor.close()
    commentsToUpdate = []
    cursor = cnx.cursor()
    cursor.execute("select Comments.commentText,Comments.jiraKey,Comments.jiraCommentId, Incidents.internalStatusPageId"
                   " from Comments inner join Incidents on Comments.jiraKey=Incidents.jiraKey WHERE"
                   " Comments.internalCommentId IS NULL AND Incidents.visability in ('External','Meltwater')")
    addLog(cursor.statement,"DBEntry")
    for (commentText,jiraKey,jiraCommentId,internalStatusPageId) in cursor:
        commentToAdd = Comment(commentText,jiraKey,jiraCommentId,internalStatusPageId)
        commentsToUpdate.append(commentToAdd)
    cursor.close()
    for thisCommentToUpdate in commentsToUpdate:
        patchData = []
        patchData.append(str("incident[message]="+thisCommentToUpdate.commentText))
        postResponse = patchHTTPS("https://api.statuspage.io/v1/pages/"+pageId+"/incidents/"+thisCommentToUpdate.internalStatusPageId+".json",apiKey,patchData)
        postResponseObj = json.loads(postResponse)
        internalId = postResponseObj["incident_updates"][0]["id"]
        cursor = cnx.cursor()
        cursor.execute(("UPDATE Comments Set internalCommentId=%s,internalTime=CURRENT_TIMESTAMP where jiraKey=%s AND"
                           " jiraCommentId=%s"),(internalId,thisCommentToUpdate.jiraKey,thisCommentToUpdate.jiraCommentId))
        addLog(cursor.statement,"DBEntry")
        cursor.close()
    cnx.close()
#If there is a runtime error, log it
except Exception as inst:
    addLog("ERROR: "+unicode(inst),"Error")

